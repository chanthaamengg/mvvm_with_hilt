package com.example.basicmvvm.model.user

import com.google.gson.annotations.SerializedName

data class UserRespone(
    @SerializedName("id")
    var id:String?=null,
    @SerializedName("name")
    var name:String?=null,
    @SerializedName("email")
    var email:String?=null,
    @SerializedName("avatar")
    var avatar:String?=null,
)

