package com.example.basicmvvm.remote.datasource

import com.example.basicmvvm.model.user.UserRespone
import retrofit2.Response

interface IDatasource {
    suspend fun getUser(): Response<List<UserRespone>>
}