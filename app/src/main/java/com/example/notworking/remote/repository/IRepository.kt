package com.example.basicmvvm.remote.repository

import com.example.basicmvvm.model.user.UserRespone
import retrofit2.Response

interface IRepository {
    suspend fun getUser(): Response<List<UserRespone>>
}