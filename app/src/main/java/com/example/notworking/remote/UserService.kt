package com.example.basicmvvm.remote

import com.example.basicmvvm.model.user.UserRespone
import retrofit2.Response
import retrofit2.http.GET

interface UserService {
    @GET("/users")
    suspend fun getUser():Response<List<UserRespone>>
}