package com.example.basicmvvm.remote.repository

import com.example.basicmvvm.model.user.UserRespone
import com.example.basicmvvm.remote.datasource.IDatasource
import retrofit2.Response
import javax.inject.Inject

class RepositoryImp @Inject constructor(
    private val datasouce:IDatasource
):IRepository {
    override suspend fun getUser(): Response<List<UserRespone>> {
        return datasouce.getUser()
    }
}