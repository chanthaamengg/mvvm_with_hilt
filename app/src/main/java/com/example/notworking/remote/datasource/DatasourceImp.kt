package com.example.basicmvvm.remote.datasource

import com.example.basicmvvm.model.user.UserRespone
import com.example.basicmvvm.remote.UserService
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class DatasourceImp @Inject constructor(
    private val userService: UserService
) : IDatasource {

    override suspend fun getUser(): Response<List<UserRespone>> {
        return userService.getUser()
    }
}