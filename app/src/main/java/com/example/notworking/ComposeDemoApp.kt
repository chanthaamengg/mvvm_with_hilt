package com.example.notworking

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * @since  4/27/2023
 * @author AOS-Molika
 */
@HiltAndroidApp
class ComposeDemoApp: Application()