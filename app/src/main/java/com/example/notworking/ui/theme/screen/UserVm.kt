package com.example.basicmvvm.ui.screen

import android.util.Log
import android.widget.RemoteViews.RemoteResponse
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.basicmvvm.model.user.UserRespone
import com.example.basicmvvm.remote.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class UserVm @Inject constructor(private val userRepo:IRepository) : ViewModel() {
    private val _users = MutableLiveData<Response<List<UserRespone>>>()
    val users:LiveData<Response<List<UserRespone>>> get() = _users

    fun fetchUser(){
        viewModelScope.launch {
            val response = userRepo.getUser().body()?.get(0)?.name
            Log.d(">>", "Response: $response")
            _users.postValue(userRepo.getUser())

        }
    }

}