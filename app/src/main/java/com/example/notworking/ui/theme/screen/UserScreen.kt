package com.example.basicmvvm.ui.screen

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.hilt.navigation.compose.hiltViewModel

import androidx.lifecycle.Observer

@Composable
fun UserScreen(
    userVm: UserVm = hiltViewModel()
){

userVm.fetchUser()

}

