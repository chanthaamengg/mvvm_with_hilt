package com.example.basicmvvm.di

import com.example.basicmvvm.connst.Const
import com.example.basicmvvm.remote.UserService
import com.example.basicmvvm.remote.datasource.DatasourceImp
import com.example.basicmvvm.remote.datasource.IDatasource
import com.example.basicmvvm.remote.repository.IRepository
import com.example.basicmvvm.remote.repository.RepositoryImp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import javax.sql.DataSource


@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }
    @Singleton
    @Provides
    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Const.BASE_URL)
            .client(client)
            .build()
    }
    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): UserService {
        return retrofit.create(UserService::class.java)
    }

    @Singleton
    @Provides
    fun provideDataSource(userService: UserService):IDatasource{
        return DatasourceImp(userService)
    }
    @Singleton
    @Provides
    fun provideLoginRepo(dataSource: IDatasource): IRepository {
        return RepositoryImp(dataSource)
    }
}